#include <vlc_common.h>
#include <vlc_vector.h>
#include <vlc_url.h>

static char*
GetCurrentTime();

static char**
ListParentFolders(const char *psz_uri, int *p_count);

struct play_rule{
    char *start;
    char *end;
    char *weekdays;
    char *path;
    char **includes;
    char **excludes;
};

struct play_rules
{
    struct VLC_VECTOR(struct play_rule) vec;
};

typedef struct play_rules play_rules_t;

typedef struct play_rule play_rule_t;

size_t
play_rules_Count(play_rules_t *list);

play_rule_t *
play_rules_Get(play_rules_t *list, size_t index);

void
play_rules_Delete(play_rules_t *list);

static char*
tokenize(char *pattern);

static short
CheckPlayRules(libvlc_int_t *p_intf, const char *psz_url);

static char** ListParentFolders(const char *psz_uri, int *i_parentFolders)
{
    char *psz_end;
    //subtract 2 leading slashes in file uri
    *i_parentFolders=-2;
    for (int i=0; psz_uri[i]; i++)
    {
        if (psz_uri[i]=='/')
        {
            (*i_parentFolders)++;
        }
    }
    char **psz_parents= malloc(sizeof(char*)*(*i_parentFolders)+1);
    //reserve space to add file name "rule.mrp" to file uri
    psz_parents[0]= malloc(strlen (psz_uri) + 10);
    strcpy(psz_parents[0], psz_uri);
    psz_end = strrchr( psz_parents[0], '/' );
    if (psz_end)
        *psz_end = '\0';
    for(int i=1; i<(*i_parentFolders); i++)
    {
        psz_parents[i] = malloc(strlen (psz_parents[i-1]) + 10);
        strcpy(psz_parents[i], psz_parents[i-1]);
        psz_end = strrchr( psz_parents[i], '/' );
        if (psz_end)
            *psz_end = '\0';
    }
    return psz_parents;
}

static char* GetCurrentTime(){
    char *buffer= malloc(6*sizeof (char));
    time_t now = time(0);
    struct tm * timeinfo;
    timeinfo = localtime (&now);
    strftime(buffer,30,"%H:%M",timeinfo);
    return buffer;
}


/* Returns current weekday index of "MTWTFSS" */
int GetWeekday() {
    time_t now = time(0);
    struct tm * timeinfo;
    timeinfo = localtime (&now);
    int day= timeinfo->tm_wday;
    day--;
    if(day==-1) day=6;
    return day;
}

static short CheckPlayRules(libvlc_int_t *p_intf, const char *psz_url)
{
    char *ext= strrchr(psz_url, '.');
    const char* const extensions[] = { ".asx",".b4s",".cue",".ifo",".m3u",".m3u8",".pls",".ram",".rar",".sdp",".vlc",".xspf",".wax",".wvx",".zip",".conf"};
    const char *WEEKDAYS= "MTWTFSS";
    size_t n = sizeof(extensions)/sizeof(extensions[0]);
    for(size_t i=0; i<n && ext != NULL; i++)
    {
        if(strcmp(ext, extensions[i])==0)
        {
            msg_Dbg(p_intf, "control.c: ignore checking playback rules for playlist media %s", psz_url);
            return true;
        }
    }

    play_rules_t *v_play_rules = (play_rules_t *)malloc(sizeof(*v_play_rules));
    vlc_vector_init(&v_play_rules->vec);

    char **parentFolders;
    int i_parentFolders;
    const int LINES= 256;
    char *rule_file, *folder_path, *file_path;
    FILE *file;

    if(strncmp("file:///", psz_url, strlen("file:///")) == 0)
    {
        parentFolders= ListParentFolders(psz_url, &i_parentFolders);
    }
    for(int i=0; i<i_parentFolders; i++)
    {
        rule_file= malloc(strlen(parentFolders[i])+10);
        strcpy(rule_file, parentFolders[i]);
        folder_path= vlc_uri2path(rule_file);
        strncat(rule_file, "/rule.mrl", 9);
        char *file_path= vlc_uri2path(rule_file);
        if( access( file_path, R_OK ) != -1 && ( file = fopen(file_path, "r" ) ) )
        {
            char *line= malloc(sizeof(char)*LINES);
            line[0]= '\0';

            while(line[0] != '\0' || fgets(line, 20, file))
            {
                char **includes= malloc(sizeof(char*)*LINES+1);
                char **excludes= malloc(sizeof(char*)*LINES+1);
                char *start= malloc(sizeof(char)*5);
                char *end= malloc(sizeof(char*)*5);
                char *weekdays= malloc(sizeof(char*)*7);
                //Read time expressed as hh:mm format
                sscanf(line, "%5s-%5s %7s\n", start, end, weekdays);
                if(weekdays==NULL || weekdays[0]=='\0')
                    strcpy(weekdays,"MTWTFSS");
                //line= malloc(sizeof(char)*LINES);;
                fscanf(file, "%9s\n", line);
                if(strncasecmp(line, "includes:", 9) == 0)
                {
                    int j=0;
                    while(fgets(line, LINES, file) && strncasecmp(line, "excludes:", 9)!=0)
                    {
                        line[strcspn(line, "\r\n")] = 0;
                        includes[j]=malloc(sizeof(char)*strlen(line));
                        strcpy(includes[j], line);
                        j++;
                    }
                    includes[j]= NULL;
                }
                if(strncasecmp(line, "excludes:", 9) == 0)
                {
                    int k=0;
                    while(fgets( line, LINES, file))
                    {
                        line[strcspn(line, "\r\n")] = 0;
                        if(strncasecmp(line, "**/", 3) == 0)
                        {
                            excludes[k]=malloc(sizeof(char)*strlen(line));
                            strcpy(excludes[k], line);
                            k++;
                            line[0] = '\0';
                        }
                        else
                            break;
                    }
                    excludes[k]=NULL;
                }
                play_rule_t item= {start, end, weekdays, folder_path, includes, excludes};
                vlc_vector_push(&v_play_rules->vec, item);
            }
            fclose(file);
        }
        free(rule_file);
        free(folder_path);
        free(file_path);
    }

    int result= 0;
    char *keyword, *match, *p_time;
    for(int i=0; i< play_rules_Count(&v_play_rules->vec); i++)
    {
        play_rule_t *p_rule= play_rules_Get(&v_play_rules->vec, i);
        p_time= GetCurrentTime();
        int weekday= GetWeekday();
        if(p_rule->weekdays[weekday] == WEEKDAYS[weekday])
        {
            result = 1;
            return result;
        }
        else if(strcmp(p_time, p_rule->start) > 0 && strcmp(p_time, p_rule->end) < 0)
        {
            int j=0;
            while(p_rule->includes[j])
            {
                if(strncmp("*", p_rule->includes[j], strlen("*")) == 0)
                {
                    keyword= tokenize(p_rule->includes[j]);
                    match= strstr( psz_url,keyword);
                    if(match)
                    {
                        if(keyword[0] == '\0')
                            result= 0;
                        else
                        {
                            result= 1;
                            return result;
                        }
                    }
                }
                j++;
            }
            j=0;
            while(p_rule->excludes[j])
            {
                if(strncmp("*", p_rule->excludes[j], strlen("*")) == 0)
                {
                    keyword= tokenize(p_rule->excludes[j]);
                    match= strstr( psz_url,keyword);
                    if(match)
                    {
                        if(keyword[0] == '\0')
                            result= 0;
                        else
                        {
                            result= -1;
                            return result;
                        }
                    }
                }
                j++;
            }
        }
    }

    //free memory
    free(p_time);
    vlc_vector_destroy(&v_play_rules->vec);
    free(v_play_rules);
    for(int i=0; i<i_parentFolders; i++)
    {
        free(parentFolders[i]);
    }
    free(parentFolders);

    return result;
}

static char* tokenize(char *pattern)
{
    int i=0, j =strlen(pattern);
    while(pattern[i] && (pattern[i]=='*' || pattern[i]=='/')) i++;
    if(j>0) j--;
    while(pattern[j] && (pattern[j]=='*' || pattern[j]=='/')) j--;
    if(j<i) return "";
    char *dest= malloc(j-i+2);
    memset(dest, 0, j-i+2);
    memcpy(dest, pattern+i, j-i+1);
    return dest;
}

size_t
play_rules_Count(play_rules_t *list)
{
    return list->vec.size;
}

play_rule_t *
play_rules_Get(play_rules_t *list, size_t index)
{
    return &list->vec.data[index];
}

